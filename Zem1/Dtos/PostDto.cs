﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zem1.Dtos
{
    public class PostDto
    {
        public Nullable<Guid> PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public int WriterUserId { get; set; }
    }
}
