﻿using AutoMapper;
using Zem1.Data.Models;

namespace Zem1.Dtos
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<PostDto, Post>();
            CreateMap<CommentDto, Comment>();
        }
    }
}
