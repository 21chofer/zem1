﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zem1.Dtos
{
    public class CommentDto
    {
        public Nullable<Guid> CommentId { get; set; }
        public string Content { get; set; }
        public string AuthorName { get; set; }
        public int? AuthorUserId { get; set; }
        public Guid PostId { get; set; }
    }
}
