using System;
using System.Collections.Generic;
using Zem1.Data.Models;

namespace Zem.Web.Models
{
    public class PostsViewModel
    {
        public List<Post> Posts { get; set; }
    }
}
