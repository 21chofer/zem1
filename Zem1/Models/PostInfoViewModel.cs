using System;
using System.Collections.Generic;
using Zem1.Data.Models;

namespace Zem.Web.Models
{
    public class PostInfoViewModel
    {
        public Post PostInfo { get; set; }
        public string PageType { get; set; }
    }
}
