﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Zem.Web.Models;
using Zem1.Data.Models;

namespace Zem.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> IndexAsync()
        {
            SetSessionAndBaseUrl();

            List<Post> postsList = new List<Post>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(baseUrl + "api/Post/"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    postsList = JsonConvert.DeserializeObject<List<Post>>(apiResponse);
                }
            }
            var vModel = new PostsViewModel { Posts = postsList };

            return View(vModel);
        }

        public async Task<IActionResult> Post(Guid id)
        {
            SetSessionAndBaseUrl();

            Post postInfo = new Post();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(baseUrl + "api/Post/" + id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    postInfo = JsonConvert.DeserializeObject<Post>(apiResponse);
                    if (postInfo.Comments != null)
                    {
                        postInfo.Comments.OrderByDescending(x => x.CreatedDate);
                    }
                }
            }
            var vModel = new PostInfoViewModel { PostInfo = postInfo };

            return View(vModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
