﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Zem.Web.Models;
using Zem1.Data.Models;

namespace Zem.Web.Controllers
{
    public class WriterController : BaseController
    {
        private readonly ILogger<WriterController> _logger;

        public WriterController(ILogger<WriterController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> IndexAsync(int id)
        {
            SetSessionAndBaseUrl();

            List<Post> postsList = new List<Post>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(baseUrl + "api/Post/GetMyPosts/" + id ))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    postsList = JsonConvert.DeserializeObject<List<Post>>(apiResponse);
                }
            }
            var vModel = new PostsViewModel { Posts = postsList };

            return View(vModel);
        }

        public async Task<IActionResult> Write(Nullable<Guid> id = null, string pageType = "create")
        {
            SetSessionAndBaseUrl();
            var vModel = new PostInfoViewModel {PageType = pageType };

            if (pageType == "edit")
            {
                Post postInfo = new Post();
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync("http://localhost:50219/api/Post/" + id))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        postInfo = JsonConvert.DeserializeObject<Post>(apiResponse);
                    }
                }
                vModel.PostInfo = postInfo;
            }
            return View(vModel);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
