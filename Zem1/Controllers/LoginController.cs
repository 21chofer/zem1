﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Zem.Web.Models;
using Zem1.Data.Models;

namespace Zem.Web.Controllers
{
    public class LoginController : BaseController
    {
        private readonly ILogger<LoginController> _logger;

        const string SessionUserId = "_UserId";
        const string SessionUsername = "_Username";
        const string SessionRole = "_Role";

        public LoginController(ILogger<LoginController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            SetSessionAndBaseUrl();

            ViewData["isValid"] = "";
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> LoginAsync(string username, string password)
        {
            SetSessionAndBaseUrl();

            User user = new User();
            using (var httpClient = new HttpClient())
            {
                var stringContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", username),
                    new KeyValuePair<string, string>("password", password),
                });
                var parameters = new Dictionary<string, string> { { "username", username }, { "password", password } };
                var encodedContent = new FormUrlEncodedContent(parameters);
                var url = $"{baseUrl}api/Login/ValidateUser?username={username}&password={password}";

                using (var response = await httpClient.PostAsync(url, null))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    user = JsonConvert.DeserializeObject<User>(apiResponse);

                    if (user != null)
                    {
                        HttpContext.Session.SetInt32(SessionUserId, user.UserId);
                        HttpContext.Session.SetString(SessionUsername, user.Username);

                        if (user.Role.RoleId == 2)
                        {
                            HttpContext.Session.SetString(SessionRole, "writer");
                            return Redirect("../Writer/Index/" + user.UserId);
                        }
                        else if (user.Role.RoleId == 3)
                        {
                            HttpContext.Session.SetString(SessionRole, "editor");
                            return Redirect("../Editor/Index/" + user.UserId);
                        }
                        else
                        {
                            HttpContext.Session.SetString(SessionRole, "normal");
                            return Redirect("../Home/");
                        }

                    }
                }
            }

            ViewData["isValid"] = "no";
            return View("Index");
        }

        public IActionResult Logout()
        {
            HttpContext.Session.SetInt32(SessionUserId, 0);
            HttpContext.Session.SetString(SessionUsername, "");
            HttpContext.Session.SetString(SessionRole, "");

            return Redirect("../Home/");
        }

    }
}
