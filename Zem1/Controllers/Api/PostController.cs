﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zem1.Data.Models;
using Zem1.Dtos;
using Zem1.Services.Interfaces;

namespace Zem1.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;
        private readonly IMapper _mapper;

        public PostController(IMapper mapper, IPostService postService)
        {
            _mapper = mapper;
            _postService = postService;
        }

        [HttpGet("{postId}")]
        public IActionResult Get(Guid postId)
        {
            return Ok(_postService.Get(postId));
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_postService.GetApproved());
        }

        [HttpGet("GetPending")]
        public IActionResult GetPending()
        {
            return Ok(_postService.GetPending());
        }

        [HttpGet("GetMyPosts/{writerId}")]
        public IActionResult GetMyPosts(int writerId)
        {
            return Ok(_postService.GetMyPosts(writerId));
        }

        [HttpPost]
        public async Task<IActionResult> CreatePost(PostDto postInfo)
        {
            try
            {
                var newPost = _postService.CreatePost(_mapper.Map<Post>(postInfo));
                await _postService.SaveChanges();

                return Ok(newPost);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdatePost(PostDto postInfo)
        {
            try
            {
                var newPost = _postService.UpdatePost(_mapper.Map<Post>(postInfo));
                await _postService.SaveChanges();

                return Ok(newPost);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("SubmitPost")]
        public async Task<IActionResult> SubmitPost(Guid postId)
        {
            try
            {
                var post = _postService.SubmitPost(postId);
                await _postService.SaveChanges();

                return Ok(post);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("ApprovePost")]
        public async Task<IActionResult> ApprovePost(Guid postId)
        {
            try
            {
                var post = _postService.ApprovePost(postId);
                await _postService.SaveChanges();

                return Ok(post);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("RejectPost")]
        public async Task<IActionResult> RejectPost(Guid postId)
        {
            try
            {
                var post = _postService.RejectPost(postId);
                await _postService.SaveChanges();

                var poost2 = _postService.Get(post.PostId);

                return Ok(post);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("DeletePost")]
        public async Task<IActionResult> DeletePost(Guid postId)
        {
            try
            {
                var post = _postService.DeletePost(postId);
                await _postService.SaveChanges();

                return Ok(post);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}