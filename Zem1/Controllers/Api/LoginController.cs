﻿using Microsoft.AspNetCore.Mvc;
using Zem1.Data.Models;
using Zem1.Services.Interfaces;

namespace Zem1.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService _loginService;

        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpPost("ValidateUser")]
        public IActionResult ValidateUser(string username, string password)
        {
            return Ok(_loginService.ValidateUser(username, password));
        }
    }
}