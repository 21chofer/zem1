﻿
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Zem1.Data.Models;
using Zem1.Dtos;
using Zem1.Services.Interfaces;

namespace Zem1.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;

        public CommentController(IMapper mapper, ICommentService commentService)
        {
            _mapper = mapper;
            _commentService = commentService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateComment(CommentDto commentInfo)
        {
            try
            {
                var comment = _commentService.AddComment(_mapper.Map<Comment>(commentInfo));
                await _commentService.SaveChanges();

                return Ok(comment);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}