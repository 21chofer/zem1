﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Zem.Web.Models;
using Zem1.Data.Models;

namespace Zem.Web.Controllers
{
    public class BaseController : Controller
    {


        public BaseController() {


        }
        public string baseUrl; // = "http://localhost:50219/";


        const string SessionUserId = "_UserId";
        const string SessionUsername = "_Username";
        const string SessionRole = "_Role";


        public void SetSessionAndBaseUrl()
        {
            baseUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/";
            ViewBag.BaseUrl = baseUrl;
            ViewBag.SessionUserId = HttpContext.Session.GetInt32(SessionUserId);
            ViewBag.SessionUsername = HttpContext.Session.GetString(SessionUsername);
            ViewBag.SessionRole = HttpContext.Session.GetString(SessionRole);
        }
    }
}
