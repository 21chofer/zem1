﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zem1.Services
{
    public enum PostStatus
    {
        draft,
        pending,
        approved,
        rejected
    }
}
