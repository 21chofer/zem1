﻿using Zem1.Data.Models;

namespace Zem1.Services.Interfaces
{
    public interface ILoginService
    {
        User ValidateUser(string username, string password);
    }
}
