﻿using System.Threading.Tasks;

namespace Zem1.Services.Interfaces
{
    public interface IBaseService
    {
        Task<int> SaveChanges();
    }
}
