﻿using Zem1.Data.Models;

namespace Zem1.Services.Interfaces
{
    public interface ICommentService : IBaseService
    {
        Comment AddComment(Comment commentInfo);
    }
}
