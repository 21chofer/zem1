﻿using System;
using System.Collections.Generic;
using Zem1.Data.Models;

namespace Zem1.Services.Interfaces
{
    public interface IPostService : IBaseService
    {
        Post Get(Guid postId);
        IEnumerable<Post> GetApproved();
        IEnumerable<Post> GetPending();
        IEnumerable<Post> GetMyPosts(int writerId);
        Post CreatePost(Post postInfo);
        Post UpdatePost(Post postInfo);
        Post SubmitPost(Guid postId);
        Post ApprovePost(Guid postId);
        Post RejectPost(Guid postId);
        Post DeletePost(Guid postId);
    }
}
