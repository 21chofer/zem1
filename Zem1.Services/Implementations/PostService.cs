﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zem1.Data;
using Zem1.Data.Models;
using Zem1.Services.Interfaces;

namespace Zem1.Services.Implementations
{
    public class PostService : IPostService
    {
        private readonly ZemDbContext _context;

        public PostService(ZemDbContext context)
        {
            _context = context;
        }

        public Post Get(Guid postId)
        {
            return _context.Posts
                .Where(x => x.PostId == postId)
                .Include("Writer")
                .Include("Comments")
                .FirstOrDefault();
        }

        public IEnumerable<Post> GetApproved()
        {
            return _context.Posts
                .Where(x => x.Status == PostStatus.approved.ToString())
                .Include("Writer")
                .Include("Comments")
                .OrderByDescending(x => x.CreatedDate);
        }

        public IEnumerable<Post> GetPending()
        {
            return _context.Posts.Where(x => x.Status == PostStatus.pending.ToString()).Include("Writer");
        }

        public IEnumerable<Post> GetMyPosts(int writerId)
        {
            return _context.Posts.Where(x => x.WriterUserId == writerId).Include("Writer").OrderByDescending(x => x.CreatedDate);
        }

        public Post CreatePost(Post postInfo)
        {
            postInfo.PostId = Guid.NewGuid();
            postInfo.CreatedDate = DateTime.Now;
            postInfo.Status = PostStatus.draft.ToString();

            _context.Posts.Add(postInfo);
            
            return postInfo;
        }

        public Post UpdatePost(Post postInfo)
        {
            var postToUpdate = _context.Posts.SingleOrDefault(x => x.PostId == postInfo.PostId);
            if (postToUpdate != null)
            {
                postToUpdate.Title = postInfo.Title;
                postToUpdate.Content = postInfo.Content;

                return postToUpdate;
            }
            else
            {
                return null;
            }
        }

        public Post SubmitPost(Guid postId)
        {
            var postToSubmit = _context.Posts.SingleOrDefault(x => x.PostId == postId);
            if (postToSubmit != null)
            {
                postToSubmit.Status = PostStatus.pending.ToString();
                postToSubmit.SubmitDate = DateTime.Now;

                return postToSubmit;
            }
            else
            {
                return null;
            }
        }

        public Post ApprovePost(Guid postId)
        {
            var postToApprove = _context.Posts.SingleOrDefault(x => x.PostId == postId);
            if (postToApprove != null)
            {
                postToApprove.Status = PostStatus.approved.ToString();
                postToApprove.ApprovalDate = DateTime.Now;

                return postToApprove;
            }
            else
            {
                return null;
            }
        }

        public Post RejectPost(Guid postId)
        {
            var postToReject = _context.Posts.SingleOrDefault(x => x.PostId == postId);
            if (postToReject != null)
            {
                postToReject.Status = PostStatus.rejected.ToString();

                return postToReject;
            }
            else
            {
                return null;
            }
        }

        public Post DeletePost(Guid postId)
        {
            var post = _context.Posts.SingleOrDefault(x => x.PostId == postId);
            if (post != null)
            {
                _context.Remove(post);

                return post;
            }
            else
            {
                return null;
            }
        }

        public async Task<int> SaveChanges()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
