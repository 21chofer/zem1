﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using Zem1.Data;
using Zem1.Data.Models;
using Zem1.Services.Interfaces;

namespace Zem1.Services.Implementations
{
    public class LoginService : ILoginService
    {
        private readonly ZemDbContext _context;

        public LoginService(ZemDbContext context)
        {
            _context = context;
        }

        public User ValidateUser(string username, string password)
        {
            return _context.Users
                .Where(x => x.Username == username && x.Password == password)
                .Include("Role")
                .FirstOrDefault();
        }
    }
}
