﻿using System;
using System.Threading.Tasks;
using Zem1.Data;
using Zem1.Data.Models;
using Zem1.Services.Interfaces;

namespace Zem1.Services.Implementations
{
    public class CommentService : ICommentService
    {
        private readonly ZemDbContext _context;

        public CommentService(ZemDbContext context)
        {
            _context = context;
        }

        public Comment AddComment(Comment commentInfo)
        {
            commentInfo.CommentId = Guid.NewGuid();
            commentInfo.CreatedDate = DateTime.Now;

            _context.Comments.Add(commentInfo);

            return commentInfo;
        }

        public async Task<int> SaveChanges()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
