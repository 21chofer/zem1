﻿# Zem1 - Fernando´s .NET Blog Engine App

Total hours: 19

Prerequisites: 
- 
- VisualStudio 2019 installed

Steps to run the app:
- 
- Open the .sln
- Open the Package Manager Console
- Run 'update-database'
- Run the application

Writer Users:
-
- Username = "BestWriter", Password = "1313"
- Username = "SuperWriter", Password = "1321"
- Username = "WriterNumber1", Password = "1111"

Editor Users:
-
- Username = "MindEditor", Password = "1234"
- Username = "TimeEditor", Password = "1234"