﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zem1.Data.Models
{
    public class Post
    {
        public Guid PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public User Writer { get; set; }
        public int WriterUserId { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
