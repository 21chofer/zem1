﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zem1.Data.Models
{
    public class Comment
    {
        public Guid CommentId { get; set; }
        public string Content { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string AuthorName { get; set; }
        public User Author { get; set; }
        public int? AuthorUserId { get; set; }
        public Post Post { get; set; }
        public Guid PostId { get; set; }
    }
}
