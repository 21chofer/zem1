﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zem1.Data.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
        public ICollection<Post> Posts { get; set; }
    }
}
