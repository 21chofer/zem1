﻿using Microsoft.EntityFrameworkCore;
using Zem1.Data.Models;

namespace Zem1.Data
{
    public class ZemDbContext : DbContext
    {
        public ZemDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var rNormal = new Role() { RoleId = 1, Name = "Normal" };
            var rWriter = new Role() { RoleId = 2, Name = "Writer" };
            var rEditor = new Role() { RoleId = 3, Name = "Editor" };

            modelBuilder.Entity<Role>().HasData(
                rNormal,
                rWriter,
                rEditor
            );

            modelBuilder.Entity<User>().HasData(
                new { UserId = 1, Username = "Thor",          Password = "1234", RoleId = 1 },
                new { UserId = 2, Username = "IronMan",       Password = "2222", RoleId = 1 },
                new { UserId = 3, Username = "Groot",         Password = "2222", RoleId = 1 },
                new { UserId = 4, Username = "BestWriter",    Password = "1313", RoleId = 2 },
                new { UserId = 5, Username = "SuperWriter",   Password = "1321", RoleId = 2 },
                new { UserId = 6, Username = "WriterNumber1", Password = "1111", RoleId = 2 },
                new { UserId = 7, Username = "MindEditor",    Password = "1234", RoleId = 3 },
                new { UserId = 8, Username = "TimeEditor",    Password = "1234", RoleId = 3 }
            );

        }
    }
}
